
import pandas as pd

def main():
    print( " --> ARTISTAS APP <--")
    menu()

def conexion_db():
    print( "INFO: Conexion a la base de datos establecida.")

def leer_artistas():
    conexion_db()
    tabla_artistas = pd.DataFrame(dict(Artistas = ["Pablo Picasso", "Lenny Kravitz", "Mikel Jackson"]))
    return tabla_artistas

def listar_artistas():
    tabla_artistas = leer_artistas()
    print(tabla_artistas)

def menu():
    print( "**********************")
    print(" - SELECIONA UNA OPCIÓN - ")
    print(" 1 - Listar artistas")
    print(" 0 - Salir")
    print( "**********************")
    opcion = input()
    if opcion == "1":
        listar_artistas()

main()